<?php

/**
 * @file
 * Gainful Employment Disclosure Templates
 *
 * This module creates custom stream wrappers and other tools for
 * listing and displaying directories of unmanaged files.
 *
 * @ingroup hfcc_modules
 *
 * @see https://drupal.org/node/560424
 * @see http://drupal7ish.blogspot.com/2011/03/using-stream-wrappers.html
 */

/**
 * Implements hook_menu().
 */
function gedt_menu() {
  $items = array();
  $items['gainful-employment'] = array(
    'title' => 'Gainful Employment',
    'page callback' => 'gedt_list_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'gedt.pages.inc',
  );
  $items['system/gainful-employment'] = array(
    'title' => 'Gainful Employment',
    'page callback' => 'file_download',
    'page arguments' => array('gedt'),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_form_alter().
 */
function gedt_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'system_file_system_settings') {
    $form['gedt_file_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Gainful Employment Disclosure file system path'),
      '#default_value' => variable_get('gedt_file_path', ''),
      '#maxlength' => 255,
      '#description' => t('An existing local file system path for storing gedt files. It should not accessible over the web. See the online handbook for <a href="@handbook">more information about securing private files</a>.', array('@handbook' => 'http://drupal.org/documentation/modules/file')),
      '#after_build' => array('system_check_directory'),
    );
  }
}

/**
 * Implements hook_block_info().
 */
function gedt_block_info() {
  return array(
    'gedt_current_list' => array(
      'info' => t('Gainful Employment Disclosure List'),
      'cache' => DRUPAL_NO_CACHE,
    ),
  );
}

/**
 * Implements hook_block_configure().
 */
function gedt_block_configure($delta = '') {
  $form = array();
  switch ($delta) {
    case 'gedt_current_list':
      module_load_include('inc', 'gedt', 'gedt.pages');
      $years = gedt_build_list(array());
      $options = array();
      foreach (array_keys($years[0]['#items']) as $year) {
        $options[$year] = $year;
      }
      $form['gedt_current_list_year'] = array(
        '#type' => 'select',
        '#title' => t('Year to display'),
        '#options' => $options,
        '#default_value' => variable_get('gedt_current_list_year', '2014'),
      );
      break;
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function gedt_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'gedt_current_list':
      variable_set('gedt_current_list_year', $edit['gedt_current_list_year']);
      break;
  }
}

/**
 * Implements hook_block_view().
 */
function gedt_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'gedt_current_list':
      if ($year = variable_get('gedt_current_list_year', NULL)) {
        module_load_include('inc', 'gedt', 'gedt.pages');
        $block['subject'] = t('Gainful Employment Information');
        $block['content'] = gedt_list_block($year);
      }
      else {
        drupal_set_message(t('You must set a year for the Gainful Employment Disclosure block.'), 'error');
      }
      break;
  }
  return $block;
}

/**
 * Implements hook_stream_wrappers().
 */
function gedt_stream_wrappers() {
  return array(
    'gedt' => array(
      'name' => t('Gainful employment disclosure files'),
      'class' => 'GedtStreamWrapper',
      'description' => t('Provides read-only paths to gainful employment disclosure template files.'),
      'type' => STREAM_WRAPPERS_READ,
    ),
  );
}

/**
 * Implements hook_file_download().
 *
 * Construct the headers to ensure the file gets downloaded
 */
function gedt_file_download($uri) {
  list($scheme, $path) = explode('://', $uri, 2);

  if ($scheme=='gedt') {
    $realpath = drupal_realpath($uri);
    if (is_file($realpath)) {
        $file = (object) array(
          'filename' => basename($path),
          'filemime' => file_get_mimetype($uri),
          'filesize' => filesize($realpath),
        );
        $headers = file_get_content_headers($file);
        return $headers;
    }
    elseif (is_dir($realpath)) {
      // This should never happen.
      return 'Path is directory.';
    }
    else {
      drupal_not_found();
    }
  }
}
