<?php

/**
 * @file
 * Contains page callbacks.
 */

/**
 * Page callback for media list.
 */
function gedt_list_page() {
  $output = array();

  $args = func_get_args();
  $title = implode(' ', array('base' => t('Gainful Employment Disclosure Archive')) + $args);
  drupal_set_title($title);
  gedt_set_breadcrumb($args);

  $output = gedt_build_list($args);
  if (!empty($output)) {
    return $output;
  }
  else {
    drupal_not_found();
  }

}

/**
 * Callback for current list block.
 */
function gedt_list_block($year) {
  // return t('Insert lookup for %year here.', array('%year' => $year));
  return gedt_build_list(array($year));
}

/**
 * Build the list.
 */
function gedt_build_list($args) {

  $target = implode('/', $args);
  $uri = 'gedt://' . $target;
  $wrapper = new GedtStreamWrapper();
  $wrapper->SetUri($uri);

  $realpath = $wrapper->realpath();
  if (is_file($realpath)) {
    drupal_goto($wrapper->GetExternalUrl());
  }
  elseif (is_dir($realpath)) {
    $dir = opendir($realpath);
    while (($entry = readdir($dir)) !== false) {
      if (!preg_match('/^\./', $entry) && !preg_match('/-print/', $entry) && !preg_match('/GedtPrint\./', $entry))
      $entries[] = $entry;
    }
    closedir($dir);

    $dirs = array();
    $files = array();
    foreach ($entries as $entry) {
      $target_entry = implode('/', $args + array('entry' => $entry));

      // For this application, we only want to scan the top-level directory,
      // which contains the years. Do not include additional subdirectories.
      // We do this by checking to verify that $args is empty.
      if (is_dir($realpath . '/' . $entry) && empty($args)) {
        $dirs[$target_entry] = l($entry, 'gainful-employment/' . $target_entry);
      }
      elseif(is_file($realpath . '/' . $entry)) {
        $filemime = file_get_mimetype($target_entry);
        $target_uri = 'system/gainful-employment/' . $target_entry;
        if (preg_match('|^text/html|', $filemime)) {

          // This is sloppy, and resource-intensive. Block or page caching should help.
          $contents = file_get_contents(realpath($realpath . '/' . $entry));
          preg_match('/dict.*\.customProgramName=(["\'])(.*)(\1)/', $contents, $matches);
          if (!empty($matches[2])) {
            $file_title = $matches[2];
          }
          else {
            preg_match('/dict.*\.programName=(["\'])(.*)(\1)/', $contents, $matches);
            if (!empty($matches[2])) {
              $file_title = $matches[2];
            }
            else {
              $file_title = $entry;
            }
          }
          $files[$file_title] = l($file_title, $target_uri);
        }
        else {
          $files[$entry] = l($entry, $target_uri);
        }
      }
    }
    if (!empty($dirs) || !empty($files)) {
      if (!empty($dirs)) {
        $output[] = array(
          '#theme' => 'item_list',
          '#items' => $dirs,
          '#attributes' => array('class' => 'archive-directories'),
        );
      }
      if (!empty($files)) {
        ksort($files, SORT_STRING);
        $output[] = array(
          '#theme' => 'item_list',
          '#items' => $files,
          '#attributes' => array('class' => 'archive-items'),
        );
      }
    }
    return $output;
  }

}

/**
 * Set the breadcrumb.
 */
function gedt_set_breadcrumb($args) {
  $crumbs = array(
    l('Home', '<front>'),
    l('Gainful Employment Disclosure', 'gainful-employment'),
  );
  $crumb_path = 'gainful-employment';
  foreach ($args as $item) {
    $crumb_path .= '/' . $item;
    $crumbs[] = l($item, $crumb_path);
  }
  // Don't include the current path (last element of the array.)
  array_pop($crumbs);
  drupal_set_breadcrumb($crumbs);
}
