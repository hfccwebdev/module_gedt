<?php

/**
 * @file
 * Stream Wrapper
 */

/**
* Default files (gedt://) stream wrapper class.
*/
class GedtStreamWrapper extends DrupalPublicStreamWrapper {
  public function getDirectoryPath() {
    return variable_get('gedt_file_path', '');
  }

  /**
   * Overrides getExternalUrl().
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('system/gainful-employment/' . $path, array('absolute' => TRUE));
  }

}
